﻿using System;

namespace Git_Proj_17
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.Write("Please enter a number");
            int a = Convert.ToInt32(Console.ReadLine());
            for (int i = 1; i <= a; i++)
            {
                Console.Write(i+" ");
            }
        }
    }
}
